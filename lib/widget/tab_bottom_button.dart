import 'package:flutter/material.dart';

class TabBottomButton extends StatelessWidget {
  final int index;
  final IconData icon;
  final int selectedIndex;
  final Function onTapPress;

  TabBottomButton(this.index, this.icon, this.selectedIndex, this.onTapPress);

  bool _isActive() {
    return index == selectedIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () => onTapPress(index),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color:
                    _isActive() ? Theme.of(context).primaryColor : Colors.white,
              ),
            ),
          ),
          height: double.infinity,
          child: Icon(
            icon,
            color: _isActive() ? Theme.of(context).primaryColor : Colors.grey,
          ),
        ),
      ),
    );
  }
}
