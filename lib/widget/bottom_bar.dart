import 'package:flutter/material.dart';

import '../widget/tab_bottom_button.dart';

class BottomBar extends StatelessWidget {
  final int currentIndex;
  final Function onTapPress;

  BottomBar(this.currentIndex, this.onTapPress);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TabBottomButton(0, Icons.av_timer, currentIndex, onTapPress),
          TabBottomButton(1, Icons.business, currentIndex, onTapPress),
          TabBottomButton(2, Icons.school, currentIndex, onTapPress),
          TabBottomButton(3, Icons.trending_up, currentIndex, onTapPress),
          TabBottomButton(4, Icons.settings, currentIndex, onTapPress),
        ],
      ),
    );

//    return BottomNavigationBar(
//      type: BottomNavigationBarType.fixed,
//      backgroundColor: Colors.white,
//      items: <BottomNavigationBarItem>[
//        buildBottomNavigationBarItem(Icons.av_timer),
//        buildBottomNavigationBarItem(Icons.business),
//        buildBottomNavigationBarItem(Icons.school),
//        buildBottomNavigationBarItem(Icons.trending_up),
//      ],
//      currentIndex: currentIndex,
//      showSelectedLabels: false,
//      showUnselectedLabels: false,
//      unselectedItemColor: Colors.black54,
//      selectedItemColor: Theme.of(context).primaryColor,
//      onTap: onTapPress,
//    );
  }

  BottomNavigationBarItem buildBottomNavigationBarItem(IconData icon) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
//      backgroundColor: Colors.white,
    );
  }
}
