import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task_app/screen/task_edit_screen.dart';

import '../widget/bottom_bar.dart';
import '../widget/circle_image.dart';
import 'dashboard_screen.dart';
import 'project_edit_screen.dart';
import 'project_screen.dart';
import 'task_screen.dart';

class BaseScreen extends StatefulWidget {
  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  int tabIndex = 0;

  Map<String, Widget> tabScreens = {
    "Dashboard": DashboardScreen(),
    "Project": ProjectScreen(),
    "Task": TaskScreen(),
    "Kanban": Center(
      child: Text("Tab 4 "),
    ),
    "All": Center(
      child: Text("Tab 5 "),
    ),
  };

  Widget _getAddScreen(int index) {
    return index == 2 ? TaskEditScreen() : ProjectEditScreen(0);
  }

  void onTabPress(int selectedIndex) {
    setState(() {
      tabIndex = selectedIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 18.0,
              width: 18.0,
              child: IconButton(
                padding: EdgeInsets.all(0.0),
                icon: const Icon(
                  Icons.add_circle_outline,
                  color: Colors.grey,
                ),
                onPressed: () {
                  showGeneralDialog(
                      context: context,
                      pageBuilder: (BuildContext buildContext,
                          Animation animation, Animation secondaryAnimation) {
                        return _getAddScreen(tabIndex);
                      });
                },
              ),
            ),
            SizedBox(
              width: 20,
            ),
            SizedBox(
              height: 18.0,
              width: 18.0,
              child: IconButton(
                padding: EdgeInsets.all(0.0),
                icon: const Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
                onPressed: () {},
              ),
            ),
            Expanded(
              child: Container(
                child: Text(
                  "${tabScreens.keys.toList()[tabIndex]}",
                  style: TextStyle(color: Colors.black54),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.notifications,
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: CircleImage(
              35,
              const NetworkImage(
                  "https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&auto=format&fit=crop&w=701&q=80"),
            ),
          ),
        ],
      ),
      body: tabScreens.values.toList()[tabIndex],
      bottomNavigationBar: BottomBar(tabIndex, onTabPress),
    );
  }
}
