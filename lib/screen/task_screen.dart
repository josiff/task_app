import 'package:flutter/material.dart';

import '../widget/task_list.dart';
import '../dummy.dart';

class TaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 10, right: 10),
      height: double.infinity,
      child: TaskList(DUMMY_TASKS),
    );
  }
}
