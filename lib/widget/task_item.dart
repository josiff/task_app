import 'package:flutter/material.dart';

import '../model/task.dart';

class TaskItem extends StatelessWidget {
  final Task task;

  TaskItem(this.task);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Container(
            width: 20,
            child: Checkbox(
              value: task.isDone,
              onChanged: (bool? value) {},
            ),
          ),
          SizedBox(width: 15),
          Expanded(
            child: Text("${task.name}"),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: Color.fromRGBO(220, 220, 220, 0.20),
            ),
            margin: EdgeInsets.all(5),
            alignment: Alignment.center,
            width: 60,
            child: task.getPriorityText(),
          )
        ],
      ),
    );
  }
}
