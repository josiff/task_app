import 'model/task.dart';
import 'model/linear_tasks.dart';
import 'model/project.dart';

final DUMMY_PROJECTS = [
  Project(1, "Envanto HTML Template", new DateTime(2029, 28, 8), 0.45),
  Project(2, "Behance Presentation", new DateTime(2020, 2, 2), 0.72),
  Project(3, "Dropbox Development", new DateTime(2019, 15, 10), 1),
  Project(4, "Airbnb App Design", new DateTime(2020, 15, 3), 0.15),
  Project(5, "Principle UI Animation", new DateTime(2020, 1, 8), 0.80),
  Project(6, "Post Event Updates", new DateTime(2020, 1, 1), 0.55),
  Project(7, "Dell", new DateTime(2018, 28, 8), 0.65),
  Project(8, "Apple sale", new DateTime(2019, 8, 8), 0.95),
];

final LINEAR_TASKS_DATA = [
  new LinearTasks(0, 30),
  new LinearTasks(1, 10),
  new LinearTasks(2, 50),
  new LinearTasks(3, 35),
  new LinearTasks(4, 75),
  new LinearTasks(5, 60),
  new LinearTasks(6, 45),
];

final DUMMY_TASKS = [
  Task("Template description", "description", Priority.Sketch, false),
  Task("Budget and contract", "description", Priority.Sketch, false),
  Task("Search For a UI kit", "description", Priority.Spotify, false),
  Task("Review created screens", "description", Priority.Dribble, true),
  Task("Dsign search page", "description", Priority.Sketch, true),
  Task("Prepare HTML & CSS", "description", Priority.Sketch, false),
  Task("Update a UI kit", "description", Priority.Spotify, false),
  Task("Browser testing", "description", Priority.Spotify, false),
  Task("Fix issues", "description", Priority.Dribble, false),
  Task("Dsign search page", "description", Priority.Spotify, false),
  Task("Search For a UI kit", "description", Priority.Dribble, false),
  Task("Dsign search page", "description", Priority.Spotify, false),
];
