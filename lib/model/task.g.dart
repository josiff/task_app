// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) => Task(
      json['name'] as String,
      json['description'] as String,
      $enumDecode(_$PriorityEnumMap, json['priority']),
      json['isDone'] as bool,
    )
      ..id = json['id'] as int?
      ..dateTo = json['dateTo'] == null
          ? null
          : DateTime.parse(json['dateTo'] as String);

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'priority': _$PriorityEnumMap[instance.priority]!,
      'isDone': instance.isDone,
      'dateTo': instance.dateTo?.toIso8601String(),
    };

const _$PriorityEnumMap = {
  Priority.Sketch: 'Sketch',
  Priority.Spotify: 'Spotify',
  Priority.Dribble: 'Dribble',
};
