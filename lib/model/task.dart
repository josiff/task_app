import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'task.g.dart';

enum Priority { Sketch, Spotify, Dribble }

@JsonSerializable()
class Task {
  int? id;
  final String name;
  final String description;
  final Priority priority;
  final bool isDone;
  DateTime? dateTo;

  Task(this.name, this.description, this.priority, this.isDone);

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);

  Map<String, dynamic> toJson() => _$TaskToJson(this);

  Text getPriorityText() {
    String text;
    if (this.priority == Priority.Dribble) {
      text = "Dribble";
    } else if (this.priority == Priority.Spotify) {
      text = "Spotify";
    } else {
      text = "Dribble";
    }

    return Text(
      text,
      style: TextStyle(color: _getPriorityColor(priority)),
    );
  }

  Color _getPriorityColor(Priority priority) {
    if (this.priority == Priority.Dribble) {
      return Colors.red;
    } else if (this.priority == Priority.Spotify) {
      return Colors.green;
    } else {
      return Colors.orange;
    }
  }
}
