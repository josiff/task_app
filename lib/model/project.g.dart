// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Project _$ProjectFromJson(Map<String, dynamic> json) => Project(
      json['id'] as int,
      json['name'] as String,
      DateTime.parse(json['dateTo'] as String),
      (json['progress'] as num).toDouble(),
    );

Map<String, dynamic> _$ProjectToJson(Project instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'dateTo': instance.dateTo.toIso8601String(),
      'progress': instance.progress,
    };
