import 'package:flutter/material.dart';

import '../dummy.dart';
import '../widget/dashboard_row.dart';
import '../widget/project_card.dart';
import '../widget/task_list.dart';
import '../widget/task_overview.dart';

class DashboardScreen extends StatelessWidget {
  static const ROUTE_NAME = "/dashboard";

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 10),
          DashBoardRow("Tasks overview", TaskOverview()),
          DashBoardRow("Pending projects", ProjectCard(DUMMY_PROJECTS[0])),
          DashBoardRow("Pending projects",
              Container(height: 400, child: TaskList(DUMMY_TASKS))),
        ],
      ),
    );
  }
}
