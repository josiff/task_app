import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

import '../model/task.dart';

class TaskEditScreen extends StatefulWidget {
  @override
  _TaskEditScreenState createState() => _TaskEditScreenState();
}

class _TaskEditScreenState extends State<TaskEditScreen> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  late Task _task;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Add new Project"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
//        color: Colors.red,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              FormBuilder(
                key: _fbKey,
//                initialValue: _task.toJson(),
                child: Column(
                  children: <Widget>[
                    FormBuilderTextField(
                      name: "description",
                      decoration: InputDecoration(hintText: "Add your description"),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required()]),
                    ),
                    FormBuilderTextField(
                      name: "comments",
                      decoration: InputDecoration(hintText: "Your message"),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required()]),
                    ),
                    FormBuilderDateTimePicker(
                      name: "dateTo",
//                      initialValue: _task.dateTo,
                      inputType: InputType.date,
                      format: DateFormat("dd. MM. yyyy"),
                      decoration:
                          InputDecoration(hintText: "Please select date"),
                      valueTransformer: (value) => value?.toString(),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4), color: Colors.blue),
                child: MaterialButton(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ADD",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  onPressed: () {
                    if (_fbKey.currentState!.saveAndValidate()) {
                      print(_fbKey.currentState!.value.toString());
                      _task = new Task.fromJson(_fbKey.currentState!.value);
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
