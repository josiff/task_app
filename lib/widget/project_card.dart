import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../model/project.dart';
import '../screen/project_edit_screen.dart';
import 'progress_bar.dart';

class ProjectCard extends StatelessWidget {
  final Project project;

  ProjectCard(this.project);

  Color _getCardColor(double progressValue) {
    if (progressValue <= 0.33) {
      return Colors.red;
    } else if (progressValue <= 0.66) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  String _getDateFormat(DateTime date) {
    var dateFormat = new DateFormat("d MMM y");
    return dateFormat.format(date);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showGeneralDialog(
          context: context,
          pageBuilder: (BuildContext buildContext, Animation animation,
              Animation secondaryAnimation) {
            return ProjectEditScreen(project.id);
          }),
      child: Card(
        margin: EdgeInsets.all(0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8),
              child: ListTile(
                leading: CircleAvatar(
                  child: Padding(
                    padding: EdgeInsets.all(5),
                    child: FittedBox(
                      child: Text('JM'),
                    ),
                  ),
                ),
                title: Text("${project.name}"),
                subtitle: Text('Due to: ${_getDateFormat(project.dateTo)}'),
              ),
            ),
            ChartBar(project.progress, _getCardColor(project.progress)),
          ],
        ),
      ),
    );
  }
}
