import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final double progressValue;
  final Color progressColor;

  ChartBar(this.progressValue, this.progressColor);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 3,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
//              border: Border.all(color: Colors.grey, width: 1.0),
              color: Color.fromRGBO(220, 220, 220, 0.20),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(4),
                  bottomRight: Radius.circular(4)),
            ),
          ),
          FractionallySizedBox(
            widthFactor: progressValue,
            child: Container(
              decoration: BoxDecoration(
                color: progressColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(4),
                  bottomRight: Radius.circular(4),
                  topRight: progressValue != 1
                      ? Radius.circular(4)
                      : Radius.circular(0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
