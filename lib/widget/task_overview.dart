import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import '../dummy.dart';
import '../model/linear_tasks.dart';

class TaskOverview extends StatelessWidget {
  Widget _buildChart() {
    return new charts.LineChart(_createSampleData(),
        defaultRenderer:
            charts.LineRendererConfig(includeArea: true, stacked: true),
        primaryMeasureAxis:
            charts.NumericAxisSpec(renderSpec: charts.NoneRenderSpec()),
        domainAxis: charts.NumericAxisSpec(renderSpec: charts.NoneRenderSpec()),
        animate: true);
  }

  List<charts.Series<LinearTasks, int>> _createSampleData() {
    return [
      new charts.Series<LinearTasks, int>(
        id: 'Desktop',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearTasks tasks, _) => tasks.year,
        measureFn: (LinearTasks tasks, _) => tasks.count,
        data: LINEAR_TASKS_DATA,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(0),
      child: Container(
        height: 86,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(width: 15),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "345",
                    style: TextStyle(fontSize: 25),
                  ),
                  Text(
                    "New tasks",
                    style: TextStyle(color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                child: _buildChart(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
