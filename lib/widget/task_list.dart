import 'package:flutter/material.dart';

import '../model/task.dart';
import '../widget/task_item.dart';

class TaskList extends StatelessWidget {
  final List<Task> tasks;

  TaskList(this.tasks);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        separatorBuilder: (context, index) => Container(
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Divider(
            height: 1,
          ),
        ),
        itemCount: tasks.length,
        itemBuilder: (context, index) => Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          child: TaskItem(tasks[index]),
        ),
      ),
    );
  }
}
