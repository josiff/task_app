import 'package:flutter/material.dart';

class DashBoardRow extends StatelessWidget {
  final String title;
  final Widget child;

  DashBoardRow(this.title, this.child);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 4,
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: 30,
            width: double.infinity,
            child: Text(
              title,
              style: TextStyle(color: Colors.grey),
            ),
          ),
          child,
        ],
      ),
    );
  }
}
