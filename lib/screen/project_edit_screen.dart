

import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';


import '../dummy.dart';
import '../model/project.dart';

class ProjectEditScreen extends StatefulWidget {
  final int projectId;

  ProjectEditScreen(this.projectId);

  @override
  _ProjectEditScreenState createState() => _ProjectEditScreenState();
}

class _ProjectEditScreenState extends State<ProjectEditScreen> {
  XFile? _storedImage;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  bool _isInit = true;
  late Project _project;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      if (widget.projectId != null) {
        _project = DUMMY_PROJECTS
            .firstWhere((element) => element.id == widget.projectId);
      } else {
        _project = Project(0, "", DateTime.now(), 0);
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future _takeImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      setState(() {
        _storedImage = image;
      });
    }
  }

  void _removeImage() {
    setState(() {
      _storedImage = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Add new Project"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
//        color: Colors.red,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              _getUploadArea(),
              FormBuilder(
                key: _fbKey,
                initialValue: _project.toJson(),
                child: Column(
                  children: <Widget>[
                    FormBuilderTextField(
                      name: "name",
                      decoration: InputDecoration(hintText: "Project name"),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required()]),
                    ),
                    FormBuilderDropdown(
                      name: "category",
                      hint: Text("Please select category"),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required()]),
                      items: ['Server', 'Web UI', 'Mobile App']
                          .map((gender) => DropdownMenuItem(
                              value: gender, child: Text("$gender")))
                          .toList(),
                    ),
                    FormBuilderDateTimePicker(
                      name: "dateTo",
                      initialValue: _project.dateTo,
                      inputType: InputType.date,
                      format: DateFormat("dd. MM. yyyy"),
                      decoration:
                          InputDecoration(hintText: "Please select date"),
                      valueTransformer: (value) => value?.toString(),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4), color: Colors.blue),
                child: MaterialButton(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ADD",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  onPressed: () {
                    if (_fbKey.currentState!.saveAndValidate()) {
                      print(_fbKey.currentState!.value.toString());
                      _project =
                          new Project.fromJson(_fbKey.currentState!.value);
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container _getUploadArea() {
    return _storedImage == null
        ? _buildUploadArea(
            null,
            DottedBorder(
              radius: Radius.circular(4),
              dashPattern: [6, 3],
              color: Color.fromRGBO(232, 236, 239, 1),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Icon(
                    Icons.cloud_upload,
                    size: 30,
                    color: Colors.grey,
                  ),
                  SizedBox(width: 10),
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      "UPLOAD IMAGE",
                      style: TextStyle(color: Colors.grey, fontSize: 10),
                    ),
                  ),
                ],
              ),
            ),
          )
        : _buildUploadArea(
            DecorationImage(
              image:  NetworkImage(_storedImage!.path),
              fit: BoxFit.cover,
            ),
            Stack(
              children: <Widget>[
                Positioned(
                  right: 0.0,
                  bottom: 0.0,
                  child: IconButton(
                    onPressed: () => _removeImage(),
                    icon: Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Container _buildUploadArea(DecorationImage? imageDecoration, Widget child) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Colors.white,
          image: imageDecoration),
      height: 150,
      child: InkWell(
        onTap: () {
          _takeImage();
        },
        child: child,
      ),
    );
  }
}
