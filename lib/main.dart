import 'package:flutter/material.dart';

import 'screen/base_screen.dart';
import 'routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Task app',
      home: BaseScreen(),
      routes: appRoutes,
    );
  }
}
