import 'screen/dashboard_screen.dart';
import 'screen/login_screen.dart';
import 'screen/project_screen.dart';

final appRoutes = {
  DashboardScreen.ROUTE_NAME: (context) => DashboardScreen(),
  LoginScreen.ROUTE_NAME: (context) => LoginScreen(),
  ProjectScreen.ROUTE_NAME: (context) => ProjectScreen(),
};
