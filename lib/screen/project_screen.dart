import 'package:flutter/material.dart';

import '../dummy.dart';
import '../widget/project_card.dart';

class ProjectScreen extends StatelessWidget {
  static const ROUTE_NAME = "/project";

  final cardItem = DUMMY_PROJECTS;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: ListView.builder(
        itemBuilder: (ctx, index) => Container(
            margin: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
            child: ProjectCard(cardItem[index])),
        itemCount: cardItem.length,
      ),
    );
  }
}
